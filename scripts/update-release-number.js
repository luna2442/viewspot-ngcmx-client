var fs = require('fs');
// grab the version from package.json
var version = require('../package.json').version;

// changing the teamcity build number  
console.log(`##teamcity[buildNumber  '${version} Release# ${process.env.BUILD_NUMBER}']` );
