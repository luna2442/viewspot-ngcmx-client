var fs = require('fs');
// grab the version from package.json
var version = require('../package.json').version;

//console.log("####process.env####### ", process.env);
var newVersion = bumpVersion(version);
updateVersion("../utility/viewspot_global/app/config.json", newVersion);
updateVersion("../utility/viewspot_global/app/config-staging.json", newVersion);
updateVersion("../package.json", newVersion);

// save a version.txt file that the bash scripts can use.
updateVersionTxtFile(newVersion);

/**
 * Bump the version
 * @param {string} version - the new version #
 */
function bumpVersion(version) {
    console.log("oldVersion", version);
    versionBits = version.split(".");
    versionBits[2] = process.env.BUILD_NUMBER ? process.env.BUILD_NUMBER : ++versionBits[2] ;
    var newVersion = versionBits.join(".");
    console.log("newVersion", newVersion);
    return newVersion
}

/**
 * Update the version on utility/viewspot_global/app/config.json
 * @param {string} version - the version #
 */
function updateVersion(configFile, version) {
    console.log("updating ", configFile, " to version ", version);
    var configContent = require(configFile);
    configContent.version = version;

   fs.writeFileSync(__dirname +"/"+ configFile, JSON.stringify(configContent, null, "    "));

}

/**
 * update a _version.txt file with the correct version
 * @param {string} version - the version #
 */
function updateVersionTxtFile( version) {
   fs.writeFileSync(__dirname +"/../version.txt", version);

}
