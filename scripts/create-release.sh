#!/bin/bash

#echo "----printenv ---"
#printenv

#CI=true npm run test &&

# ------------------------------------------------------------------------------------------
# git settings
# ------------------------------------------------------------------------------------------
# edit git url to add the username and password
newVcsrootUrl="${vcsrootUrl/$vcsrootUsername/$vcsrootUsername:$vcsrootPassword}"
git remote set-url origin $newVcsrootUrl &&
git config user.email $vcsrootEmail &&
git config user.name $vcsrootUsername &&
git config push.default simple &&

# ------------------------------------------------------------------------------------------
# update node version
# ------------------------------------------------------------------------------------------
node ./scripts/update-version.js &&
newVersion="`cat version.txt`" &&

# changing the teamcity build number
echo "##teamcity[buildNumber  '$newVersion']" &&

# ------------------------------------------------------------------------------------------
# git changes
# ------------------------------------------------------------------------------------------

# git add
git add utility/viewspot_global/app/config.json &&
git add utility/viewspot_global/app/config-staging.json &&
git add package.json &&
git add version.txt &&

# commit code
# Teamcity ignores commit messages that start with `___Release__Version___`
git commit -m"___Release__Version___$newVersion" &&

# tagging 
git tag -a "v"$newVersion -m "v"$newVersion &&

# push tags and files to origin
git push origin --follow-tags &&

# ------------------------------------------------------------------------------------------
# build
# ------------------------------------------------------------------------------------------
npm run build

