/* Customer Mobile bridge */
/* eslint-disable */

// prettier-ignore
const _bridge_={ action: function() {} };

var hasBridge = false;
try {
  if (_bridge_ == null) hasBridge = true;
} catch (e) {}

if (!hasBridge) {
  const _bridge_ = {
    action: function() {}
  };
}

//Launch an intent when the APK in question DOES NOT has an entry in the launcher DB. ()
//x represents a string value containing the package ID of the APK to be launched.
//y represents a string value containing the full activity to launch. For example for training on the Note 4: x = com.tmobile.GalaxyNote4 / y = com.tmobile.GalaxyNote4.Galaxynote4Activity
_bridge_.showDialog = function() {
  _bridge_.action(JSON.stringify([["setProps", "dialog", { visible: true }]]));
};
_bridge_.setPrefs = function(pref, val) {
  console.log("​_bridge_.setPrefs -> pref, val", pref, val);
  _bridge_.action(JSON.stringify([["setprefs", pref, val]]));
};
_bridge_.getSharedPrefs = function(pref) {};
//returns a string in the form "Latitude,Longitude"
_bridge_.getLatLong = function() {};

//CPID pref = cpid, BranchPref = branchName
_bridge_.logSharedPref = function(pref) {
  _bridge_.action(JSON.stringify([["logpref", pref]]));
};
_bridge_.explore = function() {
  _bridge_.action(JSON.stringify([["explore"]]));
};
_bridge_.relaunch = function() {
  _bridge_.action(JSON.stringify([["exit", 3000]]));
};
_bridge_.getDeviceBuild = function() {};

_bridge_.getDeviceId = function() {};

_bridge_.launchAPKtrain = function(x, y) {
  console.log("launchAPKtrain Name", x);
  console.log("launchAPKtrain Value", y);
  _bridge_.action(
    JSON.stringify([
      [
        "startActivity",
        { name: "android.intent.action.MAIN", class: { name: x, value: y } }
      ]
    ])
  );
};

//Launch an intent when the APK in question has an entry in the launcher DB. - X represents a string value containing the package ID of the APK to be launched.
_bridge_.launchAPK = function(x) {
  console.log("launchAPK", x);
  _bridge_.action(
    JSON.stringify([["startActivity", { flags: 268959744, packageName: x }]])
  );
};

//Reset the idle timer on a screen - x represents the number of minutes to reset the timer too.
_bridge_.activity = function(mul) {
  mul = mul || 1;
  _bridge_.action(
    JSON.stringify([["setProps", "idle", { delay: mul * 60000 }]])
  );
};

//Log an event. X is a string value that will be reflected in the logs.
_bridge_.log = function(x) {
  console.log("log: " + x);
  _bridge_.action(JSON.stringify([["log", x]]));
};

//Trigger a back event in the webview
_bridge_.back = function() {
  _bridge_.action(JSON.stringify([["setProps", "webview", { back: true }]]));
};

//Navigation stack functions
//Pop - this moves the user one step up in the customer mobile screen stack. Must be paired with 'push'.
_bridge_.pop = function() {
  _bridge_.action(JSON.stringify([["pop"]]));
};

//Swap - removes the current screen from the customer mobile screen stack and replaces it with the target screen. X is the string value indicating the target screen.
_bridge_.swap = function(x) {
  _bridge_.action(JSON.stringify([["swap", x]]));
};

//Push - navigate to the target screen and add the screen to the screen stack. X is a string value indicating the target screen.
_bridge_.push = function(x) {
  _bridge_.action(JSON.stringify([["push", x]]));
};

//Replace - Navigate to target screen and clear screen stack.
_bridge_.replace = function(x) {
  _bridge_.action(JSON.stringify([["replace", x]]));
};
//Close Log - Close log sessions
_bridge_.closeLogSession = function() {
  _bridge_.action(JSON.stringify([["closeLogSession"]]));
};

export default _bridge_;
