import _bridge_ from "./bridge"
import { devDeviceID, devExperienceID, devStoreID } from "./developer-settings"
import devices from "../experience/devices.json"

export const devMode = process.env.NODE_ENV === 'development' ? true : false

// build info
export const deviceID = _bridge_.getDeviceBuild() ? _bridge_.getDeviceBuild() : devDeviceID
export const experienceID = _bridge_.getSharedPrefs("experienceID", "vars") ? _bridge_.getSharedPrefs("experienceID", "vars") : devExperienceID
export const storeID = _bridge_.getSharedPrefs('storeID') ? _bridge_.getSharedPrefs('storeID') : devStoreID

// device data
export const deviceData = devices[deviceID] ? devices[deviceID] : null
export const notchAdjustments = deviceData ? deviceData.notch : null

// paths
export const hostPath = `viewspot-ngcmx-dev.smithmicro.io`
export const assetPath = `http://viewspot-client-assets.s3.amazonaws.com/`

// analytics log filter
export function log(interaction) {
  const formattedInteraction = (interaction || "unknown-interaction")
    .toLowerCase()
    .replace(/ /g, "-")
    .replace(/[^a-z0-9-]/g, "")
  _bridge_.log(formattedInteraction)
}

// refresh page url for dev
var refreshPage = true
if(devMode && refreshPage) {
  window.location = "/#/"
  refreshPage = false
}