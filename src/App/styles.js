import styled, { createGlobalStyle } from "styled-components";
import reset from "../styles/reset";
// import fonts from "../../styles/fonts";
import base from "../styles/base";

export const GlobalStyle = createGlobalStyle`
  ${reset}
  ${base}
`;

export const Container = styled.div`
  background-color: #000;
  touch-action: none;
`;
