import React, { useState, useEffect } from "react";
import * as S from "./styles";

import Setup from "../components/Setup";
import MicroSite from "../components/MicroSite";

import { deviceID } from "../utils";

export default function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [storeID, setStoreID] = useState(null);
  const [experienceID, setExperienceID] = useState(null);
  const [validNetwork, setValidNetwork] = useState(false);

  const handleSubmit = (store, experience) => {
    setIsLoggedIn(true);
    setStoreID(store);
    setExperienceID(experience);
    localStorage.setItem("storeID", store);
    localStorage.setItem("experienceID", experience);

    console.log("----------------------------------");
    console.log("BUILD INFO:");
    console.log("Device ID: " + deviceID);
    console.log("Store ID: " + store);
    console.log("Experience ID: " + experience);
    console.log("----------------------------------");
  };

  // TODO: implement logout
  // const handleLogout = () => {
  //   setStoreID("")
  //   setExperienceID("")
  //   setIsLoggedIn(false)
  //   localStorage.clear()
  // }

  useEffect(() => {
    const checkNetwork = timeout => {
      var xhr = new XMLHttpRequest();

      xhr.timeout = timeout;

      // just query something for a network check - find something else to query
      xhr.open("GET", "https://viewspot-ngcmx-dev.smithmicro.io/", true);

      xhr.ontimeout = () => {
        console.warn("Network check timed out.");
        setValidNetwork(false);
        console.log("Valid Network: " + false);
      };

      xhr.onload = () => {
        if (xhr.readyState === 4) {
          if (xhr.status >= 200 && (xhr.status < 300 || xhr.status === 304)) {
            setValidNetwork(true);
            console.log("Valid Network: " + true);
          } else {
            setValidNetwork(false);
            console.log("Valid Network: " + false);
            console.warn(xhr.statusText);
          }
        }
      };

      xhr.onerror = () => {
        console.warn("No network detected.");
        setValidNetwork(false);
        console.log("Valid Network: " + false);
      };

      xhr.send(null);
    };
    checkNetwork(5000);
  }, []);

  return (
    <S.Container>
      <S.GlobalStyle />
      {!isLoggedIn ? (
        <Setup handleSubmit={handleSubmit} validNetwork={validNetwork} />
      ) : (
        <MicroSite
          storeID={storeID}
          experienceID={experienceID}
          validNetwork={validNetwork}
        />
      )}
    </S.Container>
  );
}
