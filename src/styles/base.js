import { css } from "styled-components";

export const base = css`
  * {
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    box-sizing: border-box;
    outline: 0;
    text-rendering: optimizeLegibility;
    user-select: none;
  }

  body {
    background-color: #000;
    font-family: "Myriad Regular";
    height: 100vh;
    overflow: hidden;
    width: 100vw;
  }

  ::-webkit-scrollbar {
    display: none;
  }
`;

export default base;
