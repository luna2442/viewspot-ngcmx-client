import styled from "styled-components";

export const Container = styled.div`
    background-color: #fff;
    display: flex;
    flex-direction: column;
    height: 100vh;
    justify-content: flex-start;
    overflow: hidden;
    width: 100vw;
`;

export const SetupForm = styled.form`
    padding: 0 10vw;
    width: 100%;

    opacity: 1;
    animation-name: fadeInOpacity;
    animation-iteration-count: 1;
    animation-timing-function: ease-in;
    animation-duration: 0.75s;

    @keyframes fadeInOpacity {
        0% {
            opacity: 0;
        }
        100% {
            opacity: 1;
        }
    }
`;

export const SetupHeading = styled.h1`
    color: #005baa;
    font-size: 28px;
    margin: 25px 0 5px 0;
    text-align: center;
`;

export const SetupFormHeading = styled.h1`
    color: #736b63;
    font-size: 22px;
    margin: 20px 0;
    text-align: center;
`;

export const SetupButton = styled.input`
    background-color: #005baa;
    border-radius: 0px;
    border: 3px solid #005baa;
    color: #fff;
    margin: 20px 0;
    font-size: 20px;
    height: 55px;
    width: 100%;
`;

export const BackButton = styled.button`
    background-color: gray;
    border-radius: 0px;
    border: 3px solid gray;
    color: #fff;
    margin: 20px 0;
    text-align: center;
    font-size: 20px;
    height: 55px;
    width: 100%;
`;

export const Errors = styled.p`
    color: red;
    font-size: 20px;
    margin: 1vh 0 2vh 0;
    font-size: 20px;
    text-align: center;
    padding: 0 5vw;

    opacity: 1;
    animation-name: fadeInOpacity;
    animation-iteration-count: 1;
    animation-timing-function: ease-in;
    animation-duration: 0.25s;

    @keyframes fadeInOpacity {
        0% {
            opacity: 0;
        }
        100% {
            opacity: 1;
        }
    }
`;

export const StoreIdInput = styled.input`
    font-size: 20px;
    border: 1px solid #736b63;;
    width: 100%;
    height: 45px;
    margin: 20px 0;
    text-align: center;
`;

export const ExperienceList = styled.ul`
    border: 1px solid #736b63;;
    width: 100%;
    max-height: 40vh;
    margin: 20px 0;
    text-align: center;
    overflow: hidden;
    overflow-y: scroll;
`;

export const ExperienceButton = styled.input`
    display: inline-block;
    color: black;
    font-size: 20px;
    border: 1px solid #736b63;;
    height: 10px;
    margin: 20px 0;
    text-align: center;
`;