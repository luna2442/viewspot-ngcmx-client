import React, { useState } from "react";

//styles
import * as S from "./styles";

// dev helpers
import { hostPath } from '../../utils'

export default function Setup(props) {
  const [availableExperiences, setAvailableExperiences] = useState()
  const [storeID, setStoreID] = useState()
  const [experienceID, setExperienceID] = useState()
  const [errors, setErrors] = useState('')

  const submitStoreID = e => {
    e.preventDefault()
    if(!storeID){
      setErrors('Please enter a store ID.')
    } else {
      getAvailableExperiences()
    }
  }

  const getAvailableExperiences = () => {
    if(!props.validNetwork) {
      setErrors('Please check your internet connection and relaunch the application.')
      return
    }

    // NEED AVAILABLE EXPERIENCES FOR STORE ENDPOINT
    fetch(`https://${hostPath}/api/content/experiences/`)
    .then(response => response.json())
    .then(experiences => {
      if(errors) setErrors('')
      setAvailableExperiences(experiences)
    })
    .catch(e => {
      console.warn(e)
      setErrors('Store ID not found. Please enter a valid store ID.')
    })

    // temp until experience list is up is up
    // setErrors('')
    // setAvailableExperiences(testExperiences)
  }

  const handleStoreChange = e => {
    setStoreID(e.target.value);
  };

  const handleSelectExperience = e => {
    setExperienceID(e.target.value)
  }

  const resetForm = e => {
    e.preventDefault()
    setErrors('')
    setStoreID(null)
    setExperienceID(null)
    setAvailableExperiences(null)
  }

  const submitSetup = e => {
    e.preventDefault()
    if(!experienceID) {
      setErrors('Please select an experience.')
      return
    }
    props.handleSubmit(storeID, experienceID)
  }

  return (
    <S.Container>
      <S.SetupHeading>Setup for Viewspot</S.SetupHeading>
      {
        !availableExperiences &&
        <S.SetupForm onSubmit={submitStoreID}>
          <S.SetupFormHeading>Please Enter Your Store ID</S.SetupFormHeading>
          <S.StoreIdInput
            type="text"
            placeholder="Store ID"
            onChange={handleStoreChange}
          />
          <S.SetupButton type="submit" value="Enter" />
        </S.SetupForm>
      }
      {
        availableExperiences &&
        <S.SetupForm onSubmit={submitSetup}>
          <S.SetupFormHeading>Please Select An Experience</S.SetupFormHeading>
          <S.ExperienceList >
          {
            availableExperiences.map((experience) => {
              const stringifiedID = experience.id.toString()
              return (
                <li key={experience.id}>
                    <S.ExperienceButton type="radio" value={experience.id} onChange={handleSelectExperience} checked={stringifiedID === experienceID ? true : false} />
                    {experience.name}
                </li>
              )
            })
          }
          </S.ExperienceList>
          <S.BackButton onClick={resetForm}>Back</S.BackButton>
          <S.SetupButton type="submit" value="Continue" />
        </S.SetupForm>
      }
      {
        errors &&
        <S.Errors>{errors}</S.Errors>
      }
    </S.Container>
  )
}