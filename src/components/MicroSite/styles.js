import styled from "styled-components";

export const Container = styled.div`
  background-color: #000;
  touch-action: none;
  width: 100vw;
  height: 100vh;
`;
