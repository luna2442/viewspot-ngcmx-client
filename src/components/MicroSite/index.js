import React, { useState, useEffect } from "react";
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import testExperienceData from '../../experience/testExperience.json'

// styles
import * as S from "./styles.js";

// components
import Attract from "../Attract"
import Page from "../Page"
import Overlay from '../Overlay'

// utility/helper functions
import { log, hostPath } from "../../utils";

export default function MicroSite(props) {
  const [experienceData, setExperienceData] = useState({})
  const [hasAttract, setHasAttract] = useState(false)
  const [showAttract, setShowAttract] = useState(false)
  const [error, setError] = useState(false)

  
  // fetch experience data
  useEffect(() => {
    const fetchExperienceData = async () => {
      console.log('Fetching experience data...')
      if(props.validNetwork) {
        await fetch(`https://${hostPath}/api/v2/experiences/${props.experienceID}/`)
        .then(response => response.json())
        .then(data => {
          console.log(data)
          setExperienceData(data)
        })
        .catch(e => {
          console.warn(e)
          setError(e.message)
        })
      } else {
        await fetch(`https://${hostPath}/api/v2/experiences/${props.experienceID}/`, {cache: "force-cache"})
        .then(response => response.json())
        .then(data => {
          console.warn('Using cached data.')
          console.log(data)
          setExperienceData(data)
        })
        .catch(e => {
          console.warn(e)
          setError(e.message)
        })
      }

      // // temp until default experience is up
      // setExperienceData(testExperienceData)
      // console.log(testExperienceData) 
    }
    fetchExperienceData()
  }, [props.validNetwork])

  // check experience data
  // add attract player and show it if the experience has one
  useEffect(() => {
    if(Object.entries(experienceData).length > 0) {
      if(experienceData.attractLoop.length > 0) {
        setHasAttract(true)
        setShowAttract(true)
      }
    }
  }, [experienceData])

  const changeAttractStatus = status => {
    setShowAttract(status);
    if (status) {
      log('video-open-attract')
    } else {
      log("video-close-attract")
    }
  }

  return (
    <Router basename='/'>
      <S.Container>
        {
          hasAttract &&
          <Attract changeAttractStatus={changeAttractStatus} showAttract={showAttract} attractData={experienceData.attractLoop} />
        }
        {
          (!showAttract && Object.entries(experienceData).length > 0) &&
          <Overlay overlayData={experienceData.microsite.overlay}/>
        }
        {
          (!showAttract && Object.entries(experienceData).length > 0) &&
          <Route path='/' render={() =>
              <Switch>
                <Route exact path='/' render={() => {
                  return experienceData.microsite.landing ? <Redirect to={experienceData.microsite.landing} /> : <Redirect to={experienceData.microsite.pages[0].name} />
                }} />
                {
                  // Generate all pages/routes
                  experienceData.microsite.pages.map(page => {
                    return (
                      <Route
                        key={page.name} 
                        path={`/${page.name}`}
                        render={() => <Page pageData={page} />} 
                      />
                    )
                  })
                }
              </Switch>
            }
          />
        }
        <div style={{color: 'white'}}>{error}</div>
      </S.Container>
    </Router>
  )
}