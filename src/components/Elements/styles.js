import styled from "styled-components";

export const TextContainer = styled.div`
  position: absolute;
`;

export const ImageContainer = styled.div`
  position: absolute;
`;

export const Image = styled.img`
  width: 100%;
  height: 100%;
`;