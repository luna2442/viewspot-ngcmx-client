import React from "react"
import renderHTML from "react-render-html"

export default function TextBox(props) {
  return (
      <>
        {renderHTML(props.elementData.html)}
      </>
  )
}
