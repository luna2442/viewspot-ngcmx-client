import React from "react";
import renderHTML from "react-render-html"

import { assetPath } from "../../utils";

// styles
import * as S from "./styles";

export default function Image(props) {
  return (
    <>
      {renderHTML(props.elementData.html)}
    </>
  );
}
