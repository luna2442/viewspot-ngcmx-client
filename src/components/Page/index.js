import React, { useEffect } from "react";

import { log } from "../../utils";

// components
import CarouselPage from './PageTypes/CarouselPage'

export default function Page(props) {
  // Log analytic event on initial render
  useEffect(() => {
    log(props.pageData.analyticsKey)
  }, [props.pageData.analyticsKey]);

  const generatePage = () => {
    switch (props.pageData.type) {
      case "carousel":
        return (
          <CarouselPage pageData={props.pageData} />
        );
      default:
        console.warn(`Page failed to render - ${JSON.stringify(props.pageData)}`)
        return null;
    }
  };

  return generatePage();
}
