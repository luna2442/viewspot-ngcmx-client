import React, { useEffect } from "react";
import { log } from "../../../utils";

// external libraries
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./CarouselPage.scss";

// components
import Card from "../../Card";

export default function CarouselPage(props) {
  var isUserInteraction;

  const detectUserInteraction = () => {
    isUserInteraction = true;
  };

  // Log card analytic event
  const logCard = index => {
    if (isUserInteraction) {
      log(props.pageData.cards[index].analyticsKey);
      isUserInteraction = false;
    }
  };

  // Log analytic event on initial render
  useEffect(() => {
    // delay first card's analytic key log - show page's analytic key first
    setTimeout(() => log(props.pageData.cards[0].analyticsKey), 250);
  }, [props.pageData.cards]);

  useEffect(() => {
    // add custom event listener to arrows on carousel
    var arrows = document.getElementsByClassName("control-arrow");
    for (let i = 0; i < arrows.length; i++) {
      arrows[i].addEventListener("click", () => detectUserInteraction());
    }

    // override vertical slider indicator style
    if (props.pageData.swipeDirection === "vertical") {
      var dots = document.getElementsByClassName("control-dots")[0];
      dots.classList.add("vertical");
    }
  });

  return (
    <Carousel
      showThumbs={false}
      showStatus={false}
      swipeable={props.pageData.cards.length > 1 ? true : false}
      showIndicators={props.pageData.cards.length > 1 ? true : false}
      infiniteLoop
      autoPlay
      axis={props.pageData.swipeDirection ? props.pageData.swipeDirection : 'horizontal'}
      showArrows={false}
      interval={6000}
      onChange={index => logCard(index)}
      onSwipeMove={() => detectUserInteraction()}
    >
      {props.pageData.cards.map(card => (
        <Card key={card.name} cardData={card} />
      ))}
    </Carousel>
  );
}
