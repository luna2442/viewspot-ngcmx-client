import styled from "styled-components";

export const Overlay = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    touch-action: none;
    pointer-events: none;
    width: 100vw;
    height: 100vh;
    z-index: 100
`;

export const OverlayItem = styled.div`
    pointer-events: auto;
`;