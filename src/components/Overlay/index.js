import React from "react";
import { Link } from 'react-router-dom'

// styles
import * as S from "./styles";

// Elements
import TextBox from "../Elements/TextBox";
import Image from "../Elements/Image";

export default function Overlay(props) {
  const generateOverlay = () => {
    const overlayElements = props.overlayData.elements.map((element, index) => {
      var overlayItem
      switch (element.type) {
        case "text":
          overlayItem = <TextBox elementData={element} />;
          break;
        case "image":
          overlayItem = <Image elementData={element} />;
          break;
        default:
          overlayItem = <TextBox elementData={element} />;
          break;
      } 
      return (
        <S.OverlayItem key={index}>
            {
                element.link ? <Link to={element.link}>{overlayItem}</Link> : overlayItem
            }
        </S.OverlayItem>
      )
    });

    return <S.Overlay id="OVERLAY">{overlayElements}</S.Overlay>;
  };

  return generateOverlay();
}
