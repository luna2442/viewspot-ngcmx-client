import styled from "styled-components";

export const Container = styled.div`
  background-color: #000;
  height: ${props =>
    props.notchAdjustments && props.notchAdjustments.heightReduction
      ? `calc(100vh - ${props.notchAdjustments.heightReduction})`
      : "100vh"};
  overflow: hidden;
  position: fixed;
  visibility: ${props => (props.playing ? "visible" : "hidden")};
  width: 100vw;
  z-index: 2000;
`;

export const Video = styled.video`
  background-color: #000;
  height: ${props =>
    props.notchAdjustments && props.notchAdjustments.heightReduction
      ? `calc(100vh - ${props.notchAdjustments.heightReduction})`
      : "100vh"};
  object-fit: ${props => props.placement ? props.placement : 'cover'};
  position: absolute;
  width: 100vw;
  z-index: 2001;
`;
