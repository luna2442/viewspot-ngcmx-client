import React, { useCallback, useEffect, useRef, useState } from "react";

// styles
import * as S from "./styles";

// images
import poster from "../../images/poster.gif";

// utility/helper functions
import _bridge_ from "../../utils/bridge";
import { log, notchAdjustments, assetPath } from "../../utils";

// external libraries
import * as timesync from "timesync";

const ts = timesync.create({
  server: "http://time.da-f.us/",
  interval: 300000,
  repeat: 5
});

function Attract(props) {
  const [attractDuration, setAttractDuration] = useState();

  //idle time before attract starts
  const timeoutInMilliseconds = 60000;

  const timeoutId = useRef(null);
  const videoEl = useRef(null);

  const showAttractRef = useRef(null);
  showAttractRef.current = props.showAttract;

  const showAttractWrapper = useCallback(() => {
    props.changeAttractStatus(true);
  }, [props]);

  useEffect(() => {
    const doInactive = () => {
      if (document.getElementById("block-attract")) {
        resetTimerWrapper();
      } else if (!showAttractRef.current) {
        log("info-session-timeout");
        _bridge_.closeLogSession();
        showAttractWrapper();
      }
    };

    const resetTimerWrapper = () => {
      window.clearTimeout(timeoutId.current);
      timeoutId.current = window.setTimeout(doInactive, timeoutInMilliseconds);
    };

    const setupTimers = () => {
      document.addEventListener("keypress", resetTimerWrapper, false);
      document.addEventListener("mousedown", resetTimerWrapper, false);
      document.addEventListener("mousemove", resetTimerWrapper, false);
      document.addEventListener("touchmove", resetTimerWrapper, false);
      document.addEventListener("touchstart", resetTimerWrapper, false);
      resetTimerWrapper();
    };

    const clearDOMListeners = () => {
      document.removeEventListener("mousemove", resetTimerWrapper);
      document.removeEventListener("mousedown", resetTimerWrapper);
      document.removeEventListener("keypress", resetTimerWrapper);
      document.removeEventListener("touchmove", resetTimerWrapper);
      document.removeEventListener("touchstart", resetTimerWrapper);
    };

    if (!props.showAttract) {
      setupTimers();
    }
    return () => {
      clearDOMListeners();
    };
  }, [props.showAttract, showAttractWrapper]);

  useEffect(() => {
    const timeLoop = () => {
      setTimeout(() => {
        if (!(Math.floor(ts.now() / 100) % Math.floor(attractDuration / 100))) {
          videoEl.current.currentTime = 0;
        }
        timeLoop();
      }, 20);
    };

    if (attractDuration) {
      timeLoop();
    }
  }, [attractDuration]);

  const setVideoDuration = () => {
    setAttractDuration(Math.floor(videoEl.current.duration) * 1000);
  };

  return (
    <S.Container
      notchAdjustments={notchAdjustments}
      onClick={() => props.changeAttractStatus(false)}
      playing={props.showAttract ? true : false}
    >
      <S.Video
        autoPlay
        disableRemotePlayback
        loop
        muted
        notchAdjustments={notchAdjustments}
        onLoadedMetadata={setVideoDuration}
        poster={poster}
        ref={videoEl}
        placement={props.attractData[0].placement}
        src={`${props.attractData[0].video}`}
        type="video/mp4"
      />
    </S.Container>
  );
}

export default Attract;
