import React from "react";
import { Link } from 'react-router-dom'

// styles
import * as S from "./styles";
import carouselHelperImage from "../../../images/carousel-helper.png";

// Elements
import TextBox from "../../Elements/TextBox";
import Image from "../../Elements/Image";

// utility/helper functions
import { notchAdjustments } from "../../../utils";

export default function GenericCard(props) {
  const cardElements = props.cardData.elements.map((element, index) => {
    var cardElement
    switch (element.type) {
      case "text":
        cardElement = <TextBox key={index} elementData={element} />
        break
      case "image":
        cardElement = <Image key={index} elementData={element} />
        break
      default:
        cardElement = <TextBox key={index} elementData={element} />
    }

    return element.link ? <Link key={index} to={element.link}>{cardElement}</Link> : cardElement

  })

  return (
    <S.GenericCardContainer
      notchAdjustments={notchAdjustments}
      cardInfo={props.cardData}
    >
      {cardElements}
      <S.CarouselHelper src={carouselHelperImage} alt="required-by-carousel" />
    </S.GenericCardContainer>
  );
}
