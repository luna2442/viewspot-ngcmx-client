import styled from "styled-components";
import { assetPath } from '../../../utils'

export const GenericCardContainer = styled.div`
  height: 100vh;
  width: 100vw;
  overflow: hidden;
  position: relative;
  background-image: ${props => props.cardInfo.backgroundAsset.uri ? `url(${props.cardInfo.backgroundAsset.uri})` : null};
  background-color: ${props => props.cardInfo.styles.color};
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const CarouselHelper = styled.img`
  position: absolute;
  left: 0;
  top: 0;
  height: 1vh;
  width: 1vw !important; 
  visibility: hidden;
`;
