import React from "react";

// components
import GenericCard from "./CardTypes/GenericCard";

export default function Card(props) {
  const generateCard = () => {
    switch (props.cardData.type) {
      case "genericCard":
        return (
          <GenericCard key={props.cardData.name} cardData={props.cardData} />
        );
      default:
        return null;
    }
  };

  return generateCard();
}
