environment=$1
bucket=viewspot-client
global_package=viewspot_global

echo PACKAGES
echo ------------------
# push config and manifest to each package
cd utility/$global_package/packages
for path in ./*; do
    [ -d "${path}" ] || continue # if not a directory, skip
    package="$(basename "${path}")"
    sed -i -e "s/\"path\": \"\//\"path\": \"${environment}\//g" $package/manifest.json
    rm -rf $package/manifest-e.json
    echo Pushing config and manifest to $package
    aws s3 cp $package/ s3://$bucket/$environment/packages/$package/ --recursive --profile smartretail
done

# push html to global package
cd ../../../
echo HTML
echo ------------------
echo Preparing HTML
mv build html
zip -r html.zip html
echo Pushing html to $bucket/$environment/
aws s3 cp html.zip s3://$bucket/$environment/ --profile smartretail

# remove temp files
echo CLEANING UP
echo ------------------
rm -rf html
rm -rf html.zip
ls